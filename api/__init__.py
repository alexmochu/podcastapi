# api/__init__.py
import urllib 
import ssl

# third-party imports
from flask import Flask, jsonify, request, make_response
from flask_pymongo import PyMongo

# local imports
from config import app_config

mongo_feeds = PyMongo()

def create_app(config_name):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.config['MONGO_URI'] = "mongodb://" + urllib.parse.quote("karanja") + ":" + urllib.parse.quote("nd@M9zl7") + "@ds253398.mlab.com:53398/podcast_rss_db?retryWrites=false"
    mongo_feeds.init_app(app)
    from .controllers import controllers as controller_blueprint
    app.register_blueprint(controller_blueprint)
    

    @app.errorhandler(500)
    def internal_server_error(error):
        response = {"message" : "The server encountered an internal error. That's all we know."}
        return make_response(jsonify(response)), 500

    return app