''' controller and routes for users '''
import os
import requests
from flask import request, jsonify, make_response
from pyPodcastParser.Podcast  import Podcast
from pyPodcastParser.Item  import Item

from . import controllers
from .. import mongo_feeds

# home route
@controllers.route('/')
def home():
    response = jsonify({"message": "Welcome to Podcast API!"})
    return make_response(response), 200

@controllers.route('/feeds', methods=['GET'])
def feeds():
    if request.method == 'GET':
        feeds = mongo_feeds.db.feeds
        links = []
        for s in feeds.find():
            links.append({'links': s['links']})
        return make_response(jsonify(links)), 200

@controllers.route('/channels', methods=['GET'])
def channels():
    if request.method == 'GET':
        data = mongo_feeds.db.channels
        channels = []
        for s in data.find():
            channels.append({'channel': s['channel'], 'episode': s['episodes']})
        return make_response(jsonify(channels)), 200

@controllers.route('/channels/update', methods=['POST'])
def update_channels():
    if request.method == 'POST':
        feeds = mongo_feeds.db.feeds
        channels = mongo_feeds.db.channels
        for arr in feeds.find():
            link = 0
            while link < len(arr['links']):
                print("updating channels.....")
                rss = requests.get(arr['links'][link])
                podcast = Podcast(rss.content)
                items = podcast.items
                i = 0
                while i < len(items):
                    channels.insert({'channel': podcast.title, "episodes": [{
                        'title': items[i].title,
                        'duration': items[i].itunes_duration ,
                        'description': items[i].itunes_summary,
                        'link': items[i].link
                            }]
                        })
                    i += 1 
                link += 1
    return make_response(jsonify({"message": "channels updated succesfully"})), 200


