''' all controllers for various collections of database '''
import os
import glob
from flask import Blueprint

__all__ = [os.path.basename(f)[:-3]
    for f in glob.glob(os.path.dirname(__file__) + "/*.py")]

controllers = Blueprint('controllers', __name__)

from . import views