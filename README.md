# Podcast API

# Technology Used

The API has been built with:

- Flask micro-framework (Python 3.7)
- Mongodb(mLab)

# Features

1.  GET(Fetch) RSS feeds from the Links db in MongoDB
2.  GET(Fetch) individual Channels and their episodes
3.  Populate/Update MongoDB with channels with their episodes

# Installation

1. Ensure you have installed Python3.7+, created and an activated a virtual environment.
2. Clone the repo in your local machine inside the virtual environment you have created.
3. Navigate to the project folder(podcastapi)
4. Install all the requirements of the project by typing:
   `pip install -r requirements.txt`

# Running the API

- Type in terminal:
  `export FLASK_CONFIG=developmemt`
  `export MONGO_DBNAME=<yourdatabasename>`
  `export FLASK_APP=run.py`
  `flask run`

# Running the Tests

# API Endpoints

| Resource URL                | Methods | Description                        |
| --------------------------- | ------- | ---------------------------------- |
| /                           | GET     | Home                               |
| /feeds                      | GET     | Fetch RSS feed links               |
| /channels                   | GET     | Get channels with their episodes   |
| /channels/update            | PUT     | Populate channels with episodes    |