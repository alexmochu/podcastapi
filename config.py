# config.py
import os

class Config(object):
    """
    Common configurations
    """

    # Put any configurations here that are common across all environments
class DevelopmentConfig(Config):
    """
    Development configurations
    """

    DEBUG = True
    SQLALCHEMY_ECHO = True
    MONGO_DBNAME = os.getenv('MONGO_DBNAME')


class ProductionConfig(Config):
    """
    Production configurations
    """

    DEBUG = False
    MONGO_DBNAME = os.getenv('MONGO_DBNAME')

app_config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig
}